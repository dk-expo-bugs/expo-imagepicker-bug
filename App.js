import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import { Button, StyleSheet, Text, View } from 'react-native';

import Constants from 'expo-constants';
import React from 'react';

export default function App() {
  const handleOpenImagePicker = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);

    if (status !== 'granted') {
      console.error(status)
    }
  
    const result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      allowsMultipleSelection: false,
      quality: 1,
      aspect: [1, 1],
    });

    console.log(result)
  }

  return (
    <View style={styles.container}>
      <Button title="Open ImagePicker" onPress={handleOpenImagePicker} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
